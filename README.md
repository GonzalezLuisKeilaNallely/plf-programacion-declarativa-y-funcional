# Programación Declarativa. Orientaciones y pautas para el estudio (1998, 1999, 2001)

Mapa conceptual en PlantUML
```plantuml
@startmindmap
+[#lavender] Programación declarativa
++_ permite
+++[#plum] Programas como entradas y salidas
++++_ en lugar de
+++++[#skyblue] Unicamente datos
++++++_ dando paso a
+++++++[#Orchid] Funciones de orden superior
++_ tiene su base en
+++[#plum] la parte creativa de la programación
++++_ para 
+++++[#skyblue] Explicar algoritmos
++++++_ eliminando
+++++++[#Orchid] La cantidad de código
++++++++_ haciendo de 
+++++++++[#lightpink] La abstracción 
++++++++++[#lavender] Un proceso natural. 
++_ implica el
+++[#plum] Reemplazo de órdenes directas
++++_ en favor de
+++++[#skyblue] Descripción de aspectos fundamentales
++++++_ reduciendo
+++++++[#Orchid] El nivel de trivialidad al minimo
++++++++_ y los 
+++++++++[#lavender] problemas generados en la programación imperativa.
++++++_ Dejando las
+++++++[#Orchid] Tareas rutinarias
++++++++_ Al
+++++++++[#lavender] Compilador
+++++_ Permitiendo
++++++[#orchid] Programas más cortos y fáciles de mantener
++_ Comprende
+++[#plum] Programación funcional
++++[#skyblue] Describe funciones
+++++_ Espera una
++++++[#Orchid] Explicación de como se resuelve el problema
++++[#skyblue] No hay distincion entre datos y programas
+++++_ Haciendo posible
++++++[#orchid] Funciones que actuan sobre funciones
++++_ trata las
+++++[#skyblue] Funciones como programas
++++++_ mediante
+++++++[#Orchid] Racionamiento ecuacional
++++++++_ Abriendo paso a la
+++++++++[#lavender] Evaluación perezosa
++++++++++_ implica que
+++++++++++[#lightpink] Los cálculos no se realizan hasta ser necesarios
++++++++++++_ permitiendo
+++++++++++++[#lavender] Tipos de datos infinitos
+++[#plum] Programación lógica
++++_ trabaja con
+++++[#skyblue] Predicados lógicos
++++++_ logrando así
+++++++[#Orchid] Relaciones entre objetos
++++++_ Mediante
+++++++[#Orchid] Axiomas y leyes de inferencia
++++++++_ usando un
+++++++++[#lavender] Motor de inferencia


@endmindmap
```

[Programación Declarativa. Orientaciones @ Canal UNED](https://canal.uned.es/video/5a6f2c66b1111f54718b4911)

[Programación Declarativa. Orientaciones y pautas para el estudio  @ Canal UNED](https://canal.uned.es/video/5a6f2c5bb1111f54718b488b)

[Programación Declarativa. Orientaciones II  @ Canal UNED](https://canal.uned.es/video/5a6f2c42b1111f54718b4757)

# Lenguaje de Programación Funcional (2015)

Mapa conceptual en PlantUML
```plantuml
@startmindmap
+[#lightblue] Programación funcional.
++[#plum] No existen secuencias de control 
+++_ Ademas de
++++[#pink] Desaparecer el concepto de variable
+++_ Por lo que se usa.
++++[#pink] Recursividad
++[#plum] Transparencia referencial
+++_ Establece que
++++[#pink] Los mismos valores de entrada
+++++_ Producen los
++++++[#violet] Mismo resultado
++[#plum] Utiliza matematicas puras
+++_ por lo cual
++++[#pink] Todo es una funcion
+++++_ Puede ser de
++++++[#violet] Aplicación parcial
+++++++_ En la que.
++++++++[#violet] Un solo dato de entrada.
+++++++++_ Se puede aplicar al
++++++++++[#salmon] Resultado de otra función.
+++++_ La cual
++++++[#violet] Recibe datos de entrada.
+++++++_ Puede ser una
++++++++[#violet] Función.
+++++++++_ Siendo una.
++++++++++[#salmon] Función de orden superior.
+++++++_ Para producir una
++++++++[#violet] Salida
+++++++++_ Puede ser una
++++++++++[#salmon] Función constante
++[#plum] El orden de las operaciones.
+++_ No afecta el
++++[#pink] Resultado
+++++_ Facilita
++++++[#violet] Trabajar en paralelo
+++++++_ En los
++++++++[#violet] Sistemas multiprocesador
++[#plum] Evaluación perezosa.
+++_ En la que se realizan
++++[#pink] Calculos unicamente cuando es necesario
+++_ Puede
++++[#pink] Almacenar
+++++_ La
++++++[#violet] Evaluación de una función
+++++++_ Para no
++++++++[#salmon] Calcularla nuevamente.

@endmindmap
```

[Lenguaje de Programación Funcional @ Canal UNED](https://canal.uned.es/video/5a6f4bf3b1111f082a8b4708)
